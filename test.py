import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


df = pd.read_csv("res.csv")
df['err'] = df['err'] / 184

print(df)

print(df.keys())

# create plot
# sns.barplot(x = 'bit', y = 'err', hue = 'res', data = df,
#             palette = 'hls')
sns.boxplot(x='res', y='err', data=df)

plt.title("Taux d'erreur sur le résultat pour tout les SEU de l'espace d'erreur")
plt.show()

# g = sns.FacetGrid(df, col="res")
# g.map(sns.kdeplot, 'bit', 'err')
# plt.title("Taux d'erreur sur le résultat pour tout les SEU de l'espace d'erreur")
# plt.show()



