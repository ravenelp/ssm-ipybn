import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


sns.set_theme(style="ticks")
sns.set_style("ticks",{'axes.grid' : True})


def make_confusion_matrix(cf,
                          group_names=None,
                          categories='auto',
                          count=True,
                          percent=True,
                          cbar=True,
                          xyticks=True,
                          xyplotlabels=True,
                          sum_stats=True,
                          figsize=None,
                          cmap='Blues',
                          title=None):
    '''
    This function will make a pretty plot of an sklearn Confusion Matrix cm using a Seaborn heatmap visualization.
    Arguments
    ---------
    cf:            confusion matrix to be passed in
    group_names:   List of strings that represent the labels row by row to be shown in each square.
    categories:    List of strings containing the categories to be displayed on the x,y axis. Default is 'auto'
    count:         If True, show the raw number in the confusion matrix. Default is True.
    normalize:     If True, show the proportions for each category. Default is True.
    cbar:          If True, show the color bar. The cbar values are based off the values in the confusion matrix.
                   Default is True.
    xyticks:       If True, show x and y ticks. Default is True.
    xyplotlabels:  If True, show 'True Label' and 'Predicted Label' on the figure. Default is True.
    sum_stats:     If True, display summary statistics below the figure. Default is True.
    figsize:       Tuple representing the figure size. Default will be the matplotlib rcParams value.
    cmap:          Colormap of the values displayed from matplotlib.pyplot.cm. Default is 'Blues'
                   See http://matplotlib.org/examples/color/colormaps_reference.html
                   
    title:         Title for the heatmap. Default is None.
    '''


    # CODE TO GENERATE TEXT INSIDE EACH SQUARE
    blanks = ['' for i in range(cf.size)]

    if group_names and len(group_names)==cf.size:
        group_labels = ["{}\n".format(value) for value in group_names]
    else:
        group_labels = blanks

    if count:
        group_counts = ["{0:0.0f}\n".format(value) for value in cf.flatten()]
    else:
        group_counts = blanks

    if percent:
        group_percentages = ["{0:.2%}".format(value) for value in cf.flatten()/np.sum(cf)]
    else:
        group_percentages = blanks

    box_labels = [f"{v1}{v2}{v3}".strip() for v1, v2, v3 in zip(group_labels,group_counts,group_percentages)]
    box_labels = np.asarray(box_labels).reshape(cf.shape[0],cf.shape[1])


    # CODE TO GENERATE SUMMARY STATISTICS & TEXT FOR SUMMARY STATS
    if sum_stats:
        #Accuracy is sum of diagonal divided by total observations
        accuracy  = np.trace(cf) / float(np.sum(cf))

        #if it is a binary confusion matrix, show some more stats
        if len(cf)==2:
            #Metrics for Binary Confusion Matrices
            precision = cf[1,1] / sum(cf[:,1])
            recall    = cf[1,1] / sum(cf[1,:])
            f1_score  = 2*precision*recall / (precision + recall)
            stats_text = "\n\nAccuracy={:0.3f}\nPrecision={:0.3f}\nRecall={:0.3f}\nF1 Score={:0.3f}".format(
                accuracy,precision,recall,f1_score)
        else:
            stats_text = "\n\nAccuracy={:0.3f}".format(accuracy)
    else:
        stats_text = ""


    # SET FIGURE PARAMETERS ACCORDING TO OTHER ARGUMENTS
    figsize = plt.rcParams.get('figure.figsize')


    # MAKE THE HEATMAP VISUALIZATION
    plt.figure(figsize=figsize)
    sns.heatmap(cf,annot=box_labels,fmt="",cmap=cmap,cbar=cbar,xticklabels=categories,yticklabels=categories)

    if xyplotlabels:
        plt.ylabel('True label')
        plt.xlabel('Predicted label' + stats_text)
    else:
        plt.xlabel(stats_text)
    
    if title:
        plt.title(title)
    plt.tight_layout()


# Parse data
df_det = pd.read_csv("double_space_detection_spatial.csv")
df_det = df_det.loc[df_det['bit'] <= 71]
df_det_mat = df_det.pivot('bit', 'time', 'err').values


df_err = pd.read_csv("full_space_error.csv")
df_err_mat = df_err.pivot('bit', 'time', 'err').values

# Datas as matrix


# Only 1 in DF
# df2 = df.loc[df['err'] == 1]
# print(df2)

mat = df_err_mat * 2 + df_det_mat
nb_tp = np.sum(mat == 0)
nb_fp = np.sum(mat == 1)
nb_fn = np.sum(mat == 2)
nb_tn = np.sum(mat == 3)

print(f"{nb_tp=} {nb_fp=} {nb_fn} {nb_tn=}")

labels = ['True Neg','False Pos','False Neg','True Pos']
categories = ['no error', 'error']
make_confusion_matrix(np.array([[nb_tp, nb_fp],[nb_fn, nb_tn]]), 
                      group_names=labels,
                      categories=categories, 
                      cmap='binary')
plt.show()


plt.clf()
g = sns.JointGrid(data=df_err.loc[df_err['err'] == 1], x="time", y="bit", marginal_ticks=True)
g.plot_joint(sns.histplot, binwidth=1)
g.plot_marginals(sns.histplot, binwidth=1, element="step", color="#03012d")
plt.show()

plt.clf()
# my_colors=[(0.0, 1, 0.0), (1.0, 0.0, 0.0)]cmap=my_colors
ax = sns.heatmap(mat, cmap =sns.color_palette("Set2", 4), square=True, linewidth=0.0, linecolor=(0.1,0.2,0.2))
colorbar = ax.collections[0].colorbar
colorbar.set_ticks([0, 1, 2, 3])
colorbar.set_ticklabels(['true positive', 'false positive', 'false negative', 'true negative'])
plt.show()

# plt.show()


